'use strict';

chrome.runtime.onInstalled.addListener(function() {
  // chrome.storage.sync.set({color: '#0f0f0f'}, function() {
  //   console.log("The color is green.");
  // });
  chrome.declarativeContent.onPageChanged.removeRules(undefined, function() {
    chrome.declarativeContent.onPageChanged.addRules([{
      conditions: [new chrome.declarativeContent.PageStateMatcher({
        pageUrl: {hostEquals: 'https://*.facebook.com/*'},
      })
      ],
          actions: [new chrome.declarativeContent.ShowPageAction()]
    }]);
  });
});

// let elements = {
//   //"tab-bar" : "_2t",
//   "chat-box-bar" : ["_69pt"],
//   "chat-text-box" : ["_5wd4","_40qi","_4tdt","_6--1","_4po9","_5w-5","fbNubFlyoutFooter","_4gd0","accessible_elem","_31o4","_4gx_",
//     "_8rnv","_3e7u","_40fu","_1aa6","_5w1r","_1aa6","_3_om","_4tdx","_4tdv","_1ekr","_aou"
//   ]
// }


// let ChangeElementColor = function(e_class , e_color ){
//   let Script = 'document.querySelectorAll("[class*=\''+ e_class +'\']").forEach((item) => { \
//     item.style.background ="' + e_color + '"; \
//     item.style.borderColor ="' + e_color + '"; \
//     item.style.overflow = "hidden" ; \
//   });';
//     chrome.tabs.query({url: "https://*.facebook.com/*"}, function(tabs) {
//       chrome.tabs.executeScript(
//           tabs[0].id,
//           {code: Script});
//     });
// }

// chrome.tabs.onUpdated.addListener((tab) => {
//     chrome.tabs.query({url: "https://*.facebook.com/*"}, function(tabs) {
//       tabs.forEach((tab) => {
//         chrome.tabs.insertCSS(
//           tabs.id,
//           { file: "main.css" },
//       );
//       })
//   });
// })
// setInterval(function(){ 
//   for(let key in elements ){ 
//     let name = key;
//     let color = "#333131";
//     elements[name].forEach( (key) => {
//       ChangeElementColor(key,color)
//     });
//   }
// }, 1000);