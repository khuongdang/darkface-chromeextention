'use strict';

let elements = {
  //"tab-bar" : "_2t",
  "chat-box-bar" : ["_69pt"],
  "chat-text-box" : ["_5wd4","_40qi","_4tdt","_6--1","_4po9","_5w-5","fbNubFlyoutFooter","_4gd0","accessible_elem","_31o4","_4gx_",
    "_8rnv","_3e7u","_40fu","_1aa6","_5w1r","_1aa6","_3_om","_4tdx","_4tdv","_1ekr","_aou"
  ]
}

let changeColor = $('#changeColor');

$('.element-input').on('change',(event) => {
  let name = $(event.target).data('name');
  let color = $(event.target).val();
  elements[name].forEach( (key) => {
    ChangeElementColor(key,color)
  } )
});

// chrome.storage.sync.get('color', function(data) {
//   changeColor.style.backgroundColor = data.color;
//   changeColor.setAttribute('value', data.color);
// });

// changeColor.on('click',(element) => {  
//   for(let key in elements ){  
//     let color = $("#input-color-"+key).val() ? $("#input-color-"+key).val() : "#0f0f0f";
//     elements[key].forEach( (_key) => {
//       ChangeElementColor(_key,color)
//     })
//   };
// });

let ChangeElementColor = function(e_class , e_color ){
  let Script = 'document.querySelectorAll("[class*=\''+ e_class +'\']").forEach((item) => { \
    item.style.background ="' + e_color + '"; \
    item.style.borderColor ="' + e_color + '"; \
    item.style.overflow = "hidden" ; \
  });';
    chrome.tabs.query({active: true, currentWindow: true}, function(tabs) {
      chrome.tabs.executeScript(
          tabs[0].id,
          {code: Script});
    });
}
